
const gameWorld = document.getElementById('game');

for (let i=1; i<=81; i++) {
    gameWorld.innerHTML += "<div class = 'block' id = " + i +" ></div>"
} 

const win = document.createElement('h1')
win.setAttribute('class', 'win')
win.innerHTML = 'YOU WIN !!!'

const lose = document.createElement('h1')
lose.setAttribute('class', 'lose')
lose.innerHTML = 'GAME OVER...'

const gameWorldItem = document.getElementsByClassName('block')

const wolfs = []
const fences = []

//rabbit
const imgRabbit = document.createElement("IMG");
imgRabbit.src = "./rabbit.png" ;

const happyRabbit = document.createElement("IMG");
happyRabbit.src = "./happyRabbit.png" ;

const blood = document.createElement("IMG");
blood.src = "./blood.png" ;


let rabbitCoordinat = Math.round(Math.random()*81)
const rabbitPosition = gameWorldItem[rabbitCoordinat]
const rabbit = document.createElement('p')
rabbit.setAttribute('class', 'rabbit')
rabbit.appendChild(imgRabbit)

if (!gameWorldItem[rabbitCoordinat].innerHTML) {
    rabbitPosition.appendChild(rabbit)
}


// home
const imgHome = document.createElement("IMG");
imgHome.src = "./home.png" ;

let homeCoordinat  = Math.round(Math.random()*81)
const homePosition = gameWorldItem[homeCoordinat]
const home = document.createElement('p')
home.setAttribute('class', 'home')

if (!gameWorldItem[homeCoordinat].innerHTML) {
    homePosition.appendChild(imgHome)
}



// fence 
const imgfence = document.createElement("IMG");
imgfence.src = "./fence.png" ;

for (let i = 0; i < 5; i++) {
    renderFence()
}

function renderFence() {
    const imgfence = document.createElement("IMG");
    imgfence.src = "./fence.png" ;
    let fenceCoordinat  = Math.round(Math.random()*81)
    const fence = document.createElement('p')
    fence.setAttribute('class', 'home')
    fence.appendChild(imgfence)

    if (!gameWorldItem[fenceCoordinat].innerHTML) {
        const fencePosition = gameWorldItem[fenceCoordinat] 
        fencePosition.appendChild(fence)
    } else {
        renderFence()
        return
    }

    fences.push({
        element: fence,
        id: fenceCoordinat
    })
    
}


//wolfs
const imgWolf = document.createElement("IMG");
imgWolf.src = "./wolf.png" ;

for (let i = 0; i < 4; i++) {
    renderWolf()
}

function renderWolf() {
    const imgWolf = document.createElement("IMG");
    imgWolf.src = "./wolf.png" ;
    let wolfCoordinat =  Math.round(Math.random()*81)
    const wolfPosition = gameWorldItem[wolfCoordinat]
    const wolf = document.createElement('p')
    wolf.setAttribute('class', 'wolf')
    wolf.appendChild(imgWolf)
    if (!gameWorldItem[wolfCoordinat].innerHTML) {
        const wolfPosition = gameWorldItem[wolfCoordinat]
        wolfPosition.appendChild(wolf)
    } else {
        renderWolf()
        return
    }
    
    wolfs.push({
        element: wolf,
        id: wolfCoordinat
    })
}


function rabbitMoveToLeft() {
    if (rabbitCoordinat >= 1) {
        gameWorldItem[rabbitCoordinat].innerHTML = ''
        rabbitCoordinat = rabbitCoordinat -1
        gameWorldItem[rabbitCoordinat].appendChild(rabbit)
    } else {
        gameWorldItem[rabbitCoordinat].innerHTML = ''
        rabbitCoordinat = rabbitCoordinat + 8
        gameWorldItem[rabbitCoordinat].appendChild(rabbit)
    }
}


function rabbitMoveToRight() {
    if (rabbitCoordinat <= 79) {
        gameWorldItem[rabbitCoordinat].innerHTML = ''
        rabbitCoordinat = rabbitCoordinat + 1
        gameWorldItem[rabbitCoordinat].appendChild(rabbit)
    } else {
        gameWorldItem[rabbitCoordinat].innerHTML = ''
        rabbitCoordinat = rabbitCoordinat - 8
        gameWorldItem[rabbitCoordinat].appendChild(rabbit)
    }
}

function rabbitMoveToUp() {
    if (rabbitCoordinat >= 9) {
        gameWorldItem[rabbitCoordinat].innerHTML = ''
        rabbitCoordinat = rabbitCoordinat -9
        gameWorldItem[rabbitCoordinat].appendChild(rabbit)
    } else {
        gameWorldItem[rabbitCoordinat].innerHTML = ''
        rabbitCoordinat = rabbitCoordinat + 72
        gameWorldItem[rabbitCoordinat].appendChild(rabbit)
    }

}

function rabbitMoveToDown() {
    if (rabbitCoordinat <= 71) {
        gameWorldItem[rabbitCoordinat].innerHTML = ''
        rabbitCoordinat = rabbitCoordinat + 9
        gameWorldItem[rabbitCoordinat].appendChild(rabbit)
    } else {
        gameWorldItem[rabbitCoordinat].innerHTML = ''
        rabbitCoordinat = rabbitCoordinat - 72
        gameWorldItem[rabbitCoordinat].appendChild(rabbit)
    }
}


function wolfMove(id, wolf) {
   if (!gameWorldItem[id].innerHTML || id == rabbitCoordinat) {
        gameWorldItem[wolf.id].innerHTML = ''
        wolf.id = id
        gameWorldItem[wolf.id].appendChild(wolf.element)
        return true
    }
    return false
}


function wolfMoveToRabbit() {
    checkLose()
    for (let i = 0; i < wolfs.length; i++) {
        const wolf = wolfs[i]

        if (wolf.id > rabbitCoordinat) {

            if (wolf.id - rabbitCoordinat >= 4) {
                
                if (!wolfMove(wolf.id - 9, wolf)) {
                    wolfMove(wolf.id - 1, wolf)
                }

            } else {
                wolfMove(wolf.id - 1, wolf)
            }

        } else {
            if (rabbitCoordinat - wolf.id >= 4) {

                if (!wolfMove(wolf.id + 9, wolf)) {
                    wolfMove(wolf.id + 1, wolf)
                }

            } else {
                wolfMove(wolf.id + 1, wolf)
            }
        }
    }
}


function checkFenceRabbit(number) {
    for (let i = 0; i < fences.length; i++) {
        if (rabbitCoordinat == fences[i].id) {
            gameWorldItem[rabbitCoordinat].innerHTML = ''
            gameWorldItem[rabbitCoordinat].appendChild(imgfence)
            rabbitCoordinat = rabbitCoordinat + number
            gameWorldItem[rabbitCoordinat].appendChild(rabbit)
        } 
    }
}


function checkWin() {
    if (rabbitCoordinat == homeCoordinat) {
        homePosition.innerHTML = ''
        homePosition.appendChild(happyRabbit)
        
        setTimeout(() => {
            gameWorld.innerHTML = ''
            gameWorld.appendChild(win)
        
        }, 1000)
        
    }
}

function checkLose() {
    for (let i = 0; i < wolfs.length; i++) {
        if (wolfs[i].id == rabbitCoordinat) {
            gameWorldItem[wolfs[i].id].innerHTML = ''
            gameWorldItem[wolfs[i].id].appendChild(blood)
            
            setTimeout(() => {
                gameWorld.innerHTML = ''
                gameWorld.appendChild(lose)
            }, 1000)
            
        }  
    }
}


// addEventListener
document.addEventListener('keyup', (event) => {

    if (event.key === 'ArrowLeft') {
        rabbitMoveToLeft()
        wolfMoveToRabbit()
        checkFenceRabbit(+1)
    }

    if (event.key === 'ArrowRight') {
        rabbitMoveToRight()
        wolfMoveToRabbit()
        checkFenceRabbit(-1)
    }

    if (event.key === 'ArrowUp') {
        rabbitMoveToUp()
        wolfMoveToRabbit()
        checkFenceRabbit(9)
    }

    if (event.key === 'ArrowDown') {
        rabbitMoveToDown()
        wolfMoveToRabbit()
        checkFenceRabbit(-9)
    }

    checkLose()
    checkWin()
} )





